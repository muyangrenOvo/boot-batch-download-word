package com.muyangren.thread;

import com.deepoove.poi.XWPFTemplate;
import com.muyangren.utils.FileUtil;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.zip.ZipOutputStream;

/**
 * @author: muyangren
 * @Date: 2023/4/3
 * @Description: com.muyangren.thread
 * @Version: 1.0
 */
public class DownLoadThreadTest implements Runnable {
    private final CountDownLatch countDownLatch;
    private final ZipOutputStream zipOutputStream;
    private final Integer number;

    public DownLoadThreadTest(CountDownLatch countDownLatch, ZipOutputStream zipOutputStream, Integer number) {
        this.countDownLatch = countDownLatch;
        this.zipOutputStream = zipOutputStream;
        this.number = number;
    }

    @Override
    public void run() {

        // 获取模板
        Resource resource = new ClassPathResource("document" + File.separator + "word" + File.separator + "测试模板.docx");
        for (int i = 0; i < this.number; i++) {
            // 文件名-防止重名
            String fileName = new Date().getTime() +"-"+ UUID.randomUUID() + "《" + i + "》家人们.docx";
            try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
                // 传入参数
                HashMap<String, Object> data = new HashMap<>(3);
                data.put("one", "数据数据数据数据数据数据one");
                data.put("two", "数据数据数据数据数据数据数据数据two");
                data.put("three", "数据数据数据数据数据数据数据数据数据three");
                XWPFTemplate template = XWPFTemplate.compile(resource.getInputStream()).render(data);
                // 导出到指定路径下
                // 写入到byteArrayOutputStream
                template.write(byteArrayOutputStream);
                // 将文档写入 ByteArrayOutputStream
                synchronized (zipOutputStream) {
                    FileUtil.addToZipFile(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()), zipOutputStream, fileName);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        countDownLatch.countDown();
    }
}
